/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author OS
 */
public class Player {
    private String symbol;
    private int winCount, loseCount, drawCount;

    public Player(String symbol) {
        this.symbol = symbol;
        this.winCount = 0;
        this.loseCount = 0;
        this.drawCount = 0;
    }

    public String getSymbol() {
        return symbol;
    }

    public void win() {
        winCount++;
    }

    public void lose() {
        loseCount++;
    }

    public void draw() {
        drawCount++;
    }

    public int getWinCount() {
        return winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public int getDrawCount() {
        return drawCount;
    }

    @Override
    public String toString() {
        return "Player{" + symbol + ", winCount=" + winCount + ", loseCount=" + loseCount + ", drawCount=" + drawCount + '}';
    }
}
