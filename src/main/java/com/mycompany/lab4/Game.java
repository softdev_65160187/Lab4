package com.mycompany.lab4;

import java.util.Scanner;

public class Game{
    private Player player1;
    private Player player2;
    private Table table;
    
    public  Game(){ 
        this.player1 = new Player("O");
        this.player2 = new Player("x");
        this.table = new Table(player1,player2); 
   }
    public void newGame(){
        this.table = new Table(player1, player2);
    } 
    
    public void play(){
        showWelcome();
        while (true){
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()){
                showTable();
                showInfo();
                newGame(); 
                if (!askForRestart()) {
                    break;
                }

            }
            if(table.checkDraw()){
                printDraw();
                break;
            }
            table.switchPlayer();
            }
            newGame();
        }

    private void showWelcome() {
        System.out.println("WElcome to OX Game");
    }

    private void showTable() {
        String[][] t = table.getTable();
        for (int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void showTurn() {
        System.out.println(table.getCP().getSymbol()+" Turn");
    }

    private void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Please input row col:");
        int row = kb.nextInt();
        int col = kb.nextInt();
        table.setRowCol(row,col);
    }

    private void showInfo() {
        System.out.println("palyer1");
        System.out.println("player2");
    }

    private void printDraw() {
        System.out.println("Draw");
    }

    private boolean askForRestart() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Play again? (YES/NO): ");
        String answer = kb.next();
        return answer.equalsIgnoreCase("YES");
    }

        
}
        


