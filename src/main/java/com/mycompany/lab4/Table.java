/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author OS
 */
public class Table {
    private String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
    private Player player1;
    private Player player2;
    private Player CP;
    private int turnCount = 0;
    private int row, col;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.CP = player1;
    }

    public String[][] getTable() {
        return table;
    }

    public Player getCP() {
        return CP;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row - 1][col - 1] == "-") {
            table[row - 1][col - 1] = CP.getSymbol();
            this.row = row;
            this.col = col;
            return true;
        }
        return false;
    }

    public void switchPlayer() {
        turnCount++;
        if (CP == player1) {
            CP = player2;
        } else {
            CP = player1;
        }
    }

    public boolean checkWin() {
        if (checkRow()) {
            saveWin();
            return true;
        }if (checkCol()) {
            saveWin();
            return true;
        }if (checkD1()) {
            saveWin();
            return true;
        }if (checkD2()) {
            saveWin();
            return true;
        }return false;
    }

    private boolean checkRow() {
        String symbol = CP.getSymbol();
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != symbol) {
                return false;
            }
        }
        return true;

    }

    boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == "-") {
                    return false;
                }
            }
        }
        return true;

    }
    
    private void saveWin() {
        if (player1 == getCP()) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.win();
        }
    }

    private void saveDraw() {
        player1.draw();
        player2.draw();
    }

    private boolean checkCol() {
        String symbol = CP.getSymbol();
        for (int j = 0; j < 3; j++) {
            if (table[j][col - 1] != symbol) {
                return false;
            }
        }
        return true;

    }

    private boolean checkD1() {
        String symbol = CP.getSymbol();
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != symbol) {
                return false;
            }
        }
        return true;

    }

    private boolean checkD2() {
        String symbol = CP.getSymbol();
        for (int i = 0; i < 3; i++) {
            if (table[i][2 - i] != symbol) {
                return false;
            }
        }
        return true;

    } 
    private void printRestart() {
        System.out.println("Restart(YES/NO)");
        Scanner kb = new Scanner(System.in);
        String ans = kb.next();
        if (ans.equals("YES")) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    table[i][j] = "-";
                }
            }
        }
    }

}
